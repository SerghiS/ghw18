using UnityEngine;

namespace DefaultNamespace
{
    public class GoGravity : UnityEngine.MonoBehaviour
    {
        public float speed = 0.1f;
        private Rigidbody _rigidbody;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            _rigidbody.AddForce(Vector3.up * -speed * Time.deltaTime);
        }
    }
}