﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inst : MonoBehaviour
{

	public GameObject prebaf;
	public float speed = 0.3f;
	
		
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(Instant());
	}

	private IEnumerator Instant()
	{
		while (true)
		{
			yield return new WaitForSeconds(speed);
			Instantiate<GameObject>(prebaf,transform.position,Quaternion.identity);
		}
		
	}

	// Update is called once per frame
	void Update () 
	{
		
	}
	
}
