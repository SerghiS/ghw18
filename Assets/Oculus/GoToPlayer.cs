﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToPlayer : MonoBehaviour
{
	public float speed = 12f;

	private Rigidbody _rigidbody;
	// Use this for initialization
	void Start () 
	{
		_rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (_rigidbody != null)
			_rigidbody.AddForce((Vector3.zero - transform.position).normalized * speed * Time.deltaTime);
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.transform.tag == "player")
		{
			_rigidbody.isKinematic = false;
			_rigidbody.useGravity = true;
			_rigidbody = null;
		}
	}
}
